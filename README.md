# GitLab To-Dos ➡️ Things

Node.js script written in TypeScript, which takes the To-Dos from [GitLab](https://gitlab.org) instances and adds them to [Things](https://culturedcode.com/things) with AppleScript.

## Setup

- Copy `gitlab-instances-config.example.json` into `gitlab-instances-config.json`.
- In `gitlab-instances-config.json` set the values for your GitLab instances. `token` should be a _Personal Access Token_ with `api` permission.
- Run the script with `npm start`.

## Run the script regularly

This can be set up using macOS _Launch Agents_.

- Copy `gitlab-to-things.sh` into `~/Library/Scripts/`.
  - Edit it to update the path to the repository.
  - Update its permission: `chmod 775 gitlab-to-things.sh`.
- Copy `todosync.plist` into `~/Library/LaunchAgents/`
  - Update it to your needs (currently set to run every 5 minutes).
  - Enable it: `launchctl load -w ~/Library/LaunchAgents/todosync.plist`

Note: In both `gitlab-to-things.sh` and `todosync.plist`, you may need to replace `~` with an absolute path from the system root.
