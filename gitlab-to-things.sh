#!/bin/bash

export PATH=$PATH:/usr/local/bin:./node_modules/.bin

# Setting up nvm
export NVM_DIR="~/.nvm"
source "$NVM_DIR/nvm.sh"  # This loads nvm
nvm install 14.4.0

cd /path/to/gitlab-to-things

ts-node index.ts
