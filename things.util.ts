import runApplescript from "run-applescript";

export interface ToDoOptions {
  description?: string;
  projectName?: string;
  areaName?: string;
  activationDate?: Date;
  dueDate?: Date;
  labels?: string[];
}

export default class Things {
  public static addToDo = async (title: string, options: ToDoOptions) => {
    const { description, projectName, areaName, activationDate, dueDate, labels } = options;

    console.log(`Adding to do “${title}”...`);

    let script = `tell application "Things3"
set newToDo to make new to do with properties {name:"${title}"}
`;
    if (description) {
      script += `set notes of newToDo to "${description}"
`;
    }

    if (dueDate) {
      script += `set due date of newToDo to date "${dueDate.getDate()}.${dueDate.getMonth() + 1}.${dueDate.getFullYear()}"
`;
    }

    if (labels) {
      script += `set tag names of newToDo to "${labels.join(', ')}"
`;
    }

    if (activationDate) {
      script += `schedule newToDo for date "${activationDate.getDate()}.${activationDate.getMonth() + 1}.${activationDate.getFullYear()}"
`;
    }

    if (projectName) {
      script += `set project of newToDo to project "${projectName}"
`;
    } else if (areaName) {
      script += `set area of newToDo to area "${areaName}"
`;
    }

    script += `end tell`;

    await runApplescript(script);
    console.log(`To do added.`);
  };
}
