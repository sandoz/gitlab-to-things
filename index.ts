import { Gitlab } from '@gitbeaker/node';
import Things, { ToDoOptions } from './things.util';
import { format } from 'date-fns';
import gitLabInstancesConfig from './gitlab-instances-config.json';

function capitalize(text: string) {
  return text[0].toUpperCase() + text.slice(1);
}

function formatActionName(actionName: string) {
  return capitalize(actionName).replace(/_/g, ' ');
}

function getFirstName(name: string): string {
  if (typeof name !== 'string') {
    return 'Unknown';
  }
  return name.split(' ')[0];
}

(async () => {
  const gitLabInstances = gitLabInstancesConfig.map((gitLabConfig) => ({
    name: gitLabConfig.name,
    api: new Gitlab({
      host: gitLabConfig.host,
      token: gitLabConfig.token,
    }),
  }));

  const importToDos = async (gitLabInstance: any) => {
    console.log(`Retrieving To dos from ${gitLabInstance.name}...`);
    let toDos = (await gitLabInstance.api.Todos.all()) as any;
    console.log(`Retrieved ${toDos.length} to dos.`);
    if (toDos.length === 0) {
      console.log('Nothing to add.');
      return;
    }
    // console.log(toDos);

    console.log('Adding To dos to Things...');
    for (const toDo of toDos) {
      // console.log({
      //   title: toDo.target.title,
      //   project: toDo.project.path_with_namespace,
      //   author: toDo.author.name,
      //   action: toDo.action_name,
      //   url: toDo.target_url,
      //   body: toDo.body,
      //   targetDescription: toDo.target.description,
      //   dueDate: toDo.target.due_date
      // });

      const actionName = formatActionName(toDo.action_name);

      let description =
        `${toDo.target_url}\n\n` +
        `${actionName} by ${toDo.author.name} on ${format(
          new Date(toDo.created_at),
          'd MMMM yyyy'
        )}\n\n` +
        `${toDo.body}\n\n`;

      description = description.replace(/"/g, '\\"');

      let toDoName = toDo.target.title;
      const projectNameParts = toDo.project.path_with_namespace.split('/');
      let projectName: string | undefined = undefined;
      let areaName: string | undefined = undefined;
      const refSymbol =
        toDo.target_type === 'Issue'
          ? '#'
          : toDo.target_type === 'MergeRequest'
          ? '!'
          : '';

      const authorFirstName = getFirstName(toDo.author.name);

      if (
        actionName === 'Review requested' &&
        toDo.target_type === 'MergeRequest' &&
        toDo.target?.iid
      ) {
        toDoName = `Review !${toDo.target.iid} ${toDoName}`;
      }

      if (actionName === 'Directly addressed') {
        toDoName = `Reply to ${authorFirstName} in ${refSymbol}${toDo.target.iid} ${toDoName}`;
      }

      if (actionName === 'Mentioned') {
        toDoName = `Check ${refSymbol}${toDo.target.iid} ${toDoName} (mentioned by ${authorFirstName})`;
      }

      if (actionName === 'Build failed') {
        toDoName = `Fix build of ${refSymbol}${toDo.target.iid} ${toDoName}`;
      }

      if (actionName === 'Unmergeable') {
        toDoName = `Manually rebase ${refSymbol}${toDo.target.iid} ${toDoName}`;
      }

      if (actionName === 'Assigned') {
        if (
          toDo.target.labels?.includes('Bug') ||
          toDo.target.labels?.includes('type - bug::internal') ||
          toDo.target.labels?.includes('type - bug::client')
        ) {
          toDoName = `Fix ${refSymbol}${toDo.target.iid} ${toDoName}`;
        } else if (toDo.target.labels?.includes('User Story')) {
          toDoName = `Implement ${refSymbol}${toDo.target.iid} ${toDoName}`;
        }
      }

      if (!projectNameParts.length) {
        // We leave projectName undefined
      } else if (projectNameParts[0] === 'eval-pro') {
        projectName = 'Eval Pro';
      } else if (projectNameParts.length >= 2) {
        projectName = `${projectNameParts[0]}/${projectNameParts[1]}`;
      } else {
        // This should never happen as any GitLab project is at least in a group or owned by a user.
        toDoName = `${toDo.project.path_with_namespace}: ${toDoName}`;
      }

      const toDoOptions: ToDoOptions = {
        activationDate: new Date(),
        description,
        labels: [actionName],
        projectName,
      };

      if (toDo.target.due_date) {
        toDoOptions.dueDate = new Date(toDo.target.due_date);
      }
      toDoName = toDoName.replace(/"/g, '\\"');

      let toDoAdded = false;
      try {
        await Things.addToDo(toDoName, toDoOptions);
        toDoAdded = true;
      } catch (error) {
        console.log('Error in Apple Script when adding to do');
        console.error(error);
      }

      if (toDoAdded) {
        console.log('Marking to do as done on GitLab...');
        gitLabInstance.api.Todos.done({ todoId: toDo.id });
      }
    }
    console.log(`Done for ${gitLabInstance.name}.`);
  };

  for (const gitLabInstance of gitLabInstances) {
    await importToDos(gitLabInstance);
  }
  console.log('Done.');
})();
